# UTU Trust Social Media Connector

## Description

UTU Trust Social Media Connector handles social media connections and rewards users in UTT.

## Stacks

- Nest.js
- Redis
- Docker

## Setup locally

### 1. Install and run Redis Server

To setup this project locally, make sure to install and run Redis server:

- On Mac OS refer to ()[https://redis.io/docs/install/install-redis/install-redis-on-mac-os/]
- On Linux refer to ()[https://redis.io/docs/install/install-redis/install-redis-on-linux/]
- On Windows, refer to ()[https://redis.io/docs/install/install-redis/install-redis-on-windows/]

Check if redis is running on your system by running the following command:

```bash
redis-cli ping
```

Redis server should respond back by "PONG", which means your server is running.

### 2. Get the app id and API hash for telegram

To get the app id and API hash for telegram, which all allow the app to connect to telegram follow this documentation ()[https://core.telegram.org/api/obtaining_api_id]

### 3. Environments variables

The last step is to setup the environment variables, just copy the `env.sample` file to `.env` file

```bash
# the private key that deployed the UTT (UTU Trust Token) contract on Polygone test network
UTT_PRIVATE_KEY=b40a4e23d0d0dc6f61dae9243983908aaa089987dc90ce8a64dd8b8e4697c4ee

# UTT contract address deployed on the test polygon network
UTT_CONTRACT_ADDRESS=0xD559E16b1250c1fa22BAAc79C7CA5432835e1129

# The UTU core API url on stage
CORE_API_URL=https://stage-api.ututrust.com

# Polygone test network node URL
NODE_URL=https://rpc-mumbai.maticvigil.com

# Redis host (locally it's localhost)
REDIS_HOST=localhost

# Redis port,
# This environment variable is optional
REDIS_PORT=

# The link of your infura project
# This environment variable is optional
INFURA_WEBSOCKET=

# twitter connection credentials
# As tweetter connector is not working for now, then these environment variables are optional
TWITTER_CONSUMER_KEY=""
TWITTER_CONSUMER_SECRET=""
TWITTER_BEARER_TOKEN=""
TWITTER_REDIRECT_URL=""
TWITTER_TEST_TOKEN=""
TWITTER_TEST_SECRET=""
TWITTER_TEST_USER_ID=""

# The application PORT
PORT="8000"

# Telegram credentials
TELEGRAM_API_ID=
TELEGRAM_API_HASH=

```

## Run the application locally

We are all set, all the environment variables are configured

- Install the dependencies

```bash
npm install
```

- Run the application

```bash
npm run start:dev
```

Now you can access the application on `localhost:8000`

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## License

Nest is [ISC License (ISC)](LICENSE).
