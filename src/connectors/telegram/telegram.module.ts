import { Module } from '@nestjs/common';
import { TelegramController } from './telegram.controller';
import { TelegramService } from './telegram.service';
import { ConnectionsModule } from 'src/connections/connections.module';
import { TelegramRelationConsumer } from './telegram-relations.processor';
import { BullModule } from '@nestjs/bull';

@Module({
  imports: [
    ConnectionsModule,
    BullModule.registerQueue(
      {
        name: 'twitter-relations',
      },
      {
        name: 'save-relationship',
      },
      {
        name: 'telegram-relations',
      },
    ),
  ],
  controllers: [TelegramController],
  providers: [TelegramService, TelegramRelationConsumer],
})
export class TelegramModule {}
