import {
  IsNotEmpty,
  IsNumberString,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export class TelegramConnectionDto {
  @IsNotEmpty()
  @IsPhoneNumber()
  phone_number: string;

  @IsNotEmpty()
  @IsString()
  phone_code_hash: string;

  @IsNotEmpty()
  @IsNumberString()
  phone_code: string;

  // @IsNotEmpty()
  // @IsString()
  address: string;

  password: string;
}
