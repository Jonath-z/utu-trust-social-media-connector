import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Req,
} from '@nestjs/common';
import { TelegramConnectionDto } from './telegram.dto';
import { TelegramService } from './telegram.service';

@Controller('')
export class TelegramController {
  constructor(private readonly telegramService: TelegramService) {}

  @Post('')
  async telegram(
    @Body() connectionDto: TelegramConnectionDto,
    @Req() request: Request,
  ) {
    try {
      const telegramClientId = String(
        request.headers['utu-trust-api-client-id'],
      );
      const result = await this.telegramService.telegram(
        connectionDto,
        telegramClientId,
      );
      return result;
    } catch (e) {
      console.log(e);
      throw new HttpException(e.message, HttpStatus.PRECONDITION_FAILED);
    }
  }
}
