import { Injectable } from '@nestjs/common';
import { TelegramConnectionDto } from './telegram.dto';
import { Api } from 'telegram';
import { BigInteger } from 'big-integer';
import { InjectQueue } from '@nestjs/bull';
import axios from 'axios';
import { verifyCode } from 'src/lib/telegramAPI';
import { addConnection } from 'src/lib/ethereum';
import { Queue } from 'bull';
import { ConnectionsHelper } from '../../connections/connections.helper';

import { TELEGRAM_CONNECTION_TYPE_ID } from 'src/config';

@Injectable()
export class TelegramService {
  constructor(
    @InjectQueue('telegram-relations') private telegramRelationsQueue: Queue,
    private readonly connectionsHelper: ConnectionsHelper,
  ) {}

  async createTelegramEntity(
    user: Api.User,
    address: string,
    clientId: string,
  ) {
    return this.connectionsHelper.saveEntity(
      {
        name: user.username,
        type: 'Address',
        ids: {
          uuid: address,
          address: address,
          telegram: Number(user.id),
        },
        // image: user.photo,
        properties: {
          telegram: {
            name:
              user.username ||
              `${user.firstName || ''}  ${user.lastName || ''}`,
            // image: if available, include TG profile image
          },
        },
      },
      clientId,
    );
  }

  async createTelegramRelations(
    id: string | BigInteger,
    address: string,
    userSession: any,
    clientId: string,
  ) {
    console.log('createTelegramRelations');
    await this.telegramRelationsQueue.add({
      id,
      address,
      userSession,
      clientId,
    });
    return true;
  }

  async telegram(connectionDto: TelegramConnectionDto, telegramClientId) {
    const { userSession, user } = await verifyCode(connectionDto);
    const address = String(connectionDto.address).toLowerCase();
    await this.createTelegramEntity(user, address, telegramClientId);

    await this.createTelegramRelations(
      user.id,
      address,
      userSession,
      telegramClientId,
    );

    await addConnection(address, TELEGRAM_CONNECTION_TYPE_ID, user.id);

    return {
      message: 'Linking data successful!',
    };
  }
}
