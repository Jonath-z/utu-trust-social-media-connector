import { Module } from '@nestjs/common';
import { ConnectionsModule } from 'src/connections/connections.module';
import { TwitterController } from './twitter.controller';
import { TwitterService } from './twiiter.service';
import { TwitterRelationConsumer } from './twitter-relations.processor';
import { BullModule } from '@nestjs/bull';

@Module({
  imports: [
    ConnectionsModule,
    BullModule.registerQueue(
      {
        name: 'twitter-relations',
      },
      {
        name: 'save-relationship',
      },
      {
        name: 'telegram-relations',
      },
    ),
  ],
  controllers: [TwitterController],
  providers: [TwitterService, TwitterRelationConsumer],
})
export class TwitterModule { }
