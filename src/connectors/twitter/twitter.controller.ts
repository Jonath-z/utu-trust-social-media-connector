import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  Req,
  Res,
} from '@nestjs/common';

import { TwitterService } from './twiiter.service';
import { TwitterConnectionDto } from './twitter.dto';
import { ConnectionsHelper } from 'src/connections/connections.helper';
import { Request, Response } from 'express';

@Controller('')
export class TwitterController {
  constructor(
    private readonly twitterService: TwitterService,
    private readonly connectionsService: ConnectionsHelper,
  ) {}

  @Post('twitter')
  async twitter(
    @Body() connectionDto: TwitterConnectionDto,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const clientId = String(request.headers['utu-trust-api-client-id']);
    console.log('connecting to twitter');
    try {
      const result = await this.twitterService.twitter(connectionDto, clientId);
      res.send(result);
    } catch (e) {
      console.log(e);
      throw new HttpException(e.message, HttpStatus.PRECONDITION_FAILED);
    }
  }
}
