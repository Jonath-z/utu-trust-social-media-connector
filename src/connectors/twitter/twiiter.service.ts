import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { ConnectionsHelper } from 'src/connections/connections.helper';
import TwitterOauth, { OathCredentials } from 'src/lib/twitterOauth';
import { addConnection } from 'src/lib/ethereum';

import { TWITTER_CONNECTION_TYPE_ID } from 'src/config';
import { TwitterConnectionDto } from './twitter.dto';
import TwitterApi from 'src/lib/twitterAPI';

@Injectable()
export class TwitterService {
  constructor(
    @InjectQueue('telegram-relations') private twitterRelationsQueue: Queue,
    private readonly connectionsHelper: ConnectionsHelper,
  ) {}

  async twitter(connectionDto: TwitterConnectionDto, clientId: string) {
    const data = (await TwitterOauth.getAccessToken(connectionDto)) as any;
    const twitterId = data.results.user_id;
    const credentials = {
      token: data.oauth_access_token,
      token_secret: data.oauth_access_token_secret,
    };
    const address = String(connectionDto.address).toLowerCase();
    await this.createEntity(twitterId, address, clientId);
    await this.createRelations(credentials, twitterId, address, clientId);
    await addConnection(address, TWITTER_CONNECTION_TYPE_ID, twitterId);
    return data;
  }

  async createEntity(id: string, address: string, clientId: string) {
    const twitterData = await TwitterApi.getUser(id);
    return this.connectionsHelper.saveEntity(
      {
        name: twitterData.username,
        type: 'Address',
        ids: {
          uuid: address,
          address: address,
          twitter: twitterData.id,
        },
        image: twitterData.profile_image_url,
        properties: {
          twitter: {
            name: twitterData.username,
            image: twitterData.profile_image_url,
          },
        },
      },
      clientId,
    );
  }

  async createRelations(
    credentials: OathCredentials,
    id: string,
    address: string,
    clientId: string,
  ) {
    await this.twitterRelationsQueue.add({
      credentials,
      id,
      address,
      type: 'FOLLOWERS',
      clientId,
    });

    await this.twitterRelationsQueue.add({
      credentials,
      id,
      address,
      type: 'FOLLOWING',
      clientId,
    });
    return true;
  }
}
