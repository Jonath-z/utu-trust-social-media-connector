import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class ConnectionsHelper {
  constructor(
  ) { }

  async saveEntity(data: any, clientId: string) {
    try {
      const result = await axios.post(
        `${process.env.CORE_API_URL}/entity`,
        data,
        {
          headers: {
            'utu-trust-api-client-id': clientId,
          },
        },
      );
      console.log('saved');
      return result;
    } catch (e) {
      console.log(e);
    }
  }
}
