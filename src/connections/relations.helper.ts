import { Queue } from 'bull';

export const sendRequests = async (
  relations: any[],
  clientId: string,
  saveRelationshipQueue: Queue,
) => {
  await Promise.all(
    relations.map(async (relation) => {
      await saveRelationshipQueue.add({
        relation,
        clientId,
      });
      return relation;
    }),
  );
};
