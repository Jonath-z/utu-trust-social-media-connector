import { Module } from '@nestjs/common';
import { ConnectionsHelper } from './connections.helper';
import { BullModule } from '@nestjs/bull';
import { RelationshipConsumer } from './relationship.processor';

@Module({
  imports: [
    BullModule.registerQueue(
      {
        name: 'twitter-relations',
      },
      {
        name: 'save-relationship',
      },
      {
        name: 'telegram-relations',
      },
    ),
  ],
  controllers: [],
  providers: [ConnectionsHelper, RelationshipConsumer],
  exports: [ConnectionsHelper]
})
export class ConnectionsModule { }
